% crust_corr
function data = crust_corr(data)
disp('It is time for crustal thickness and elevation correction')
ray = data.ray;     stn = data.stn;
ray.corr1 = zeros( size(ray.p) );
unique_orid = unique( ray.orid );   
norid = length( unique_orid );    
depth = 45;         % max depthl for mean velocity values in crust file, should be...
                    % near or slightly deeper than max Moho in the array
deptho = 40;
dh =depth*tand(35); % lateral distance stn to depthl pierce-point for delta=30 event
                    % using mean depthl velocity=6.5
dho=deptho*tand(35);
dr = dh/sqrt(2);    % for ne,se,sw,nw pierce-point coordinates
dro = dho/sqrt(2);
h = waitbar( 0, ' Hello, Dr. Youssof You are computing crust corrections :) ' );

%i added this
kount=0;
for iorid = 1:norid                
	waitbar( (iorid-1)/norid, h );	
	
    indx = find( ray.orid == unique_orid(iorid) );
    sta_num = ray.sta_num( indx );
    p = ray.p( indx );  
	baz = ray.baz( indx );   
    c_x = [ 0 0  dr dh  dr   0 -dr -dh -dr ]';                
    c_y = [ 0 dh dr 0  -dr -dh -dr   0  dr ]';
    stn_z = stn.z(sta_num);
    n_z   = length(stn_z(stn_z>0));
%     
    c_xo = [ 0 0  dro dho  dro   0 -dro -dho -dro ]';                
     c_yo = [ 0 dho dro 0  -dro -dho -dro   0  dro ]';
    for istn = 1:length(indx)
        snum = sta_num(istn);
        inc = asind(p(istn)*stn.z(sta_num(istn)));  % p=sin(inc)/v -> inc = asin(p/mean(velocity))
        ray_l = depth/cosd(inc);                    % straight line stn to depthl pierce-point
        r_ppt = ray_l*sind(inc);                    % lateral distance between stn and depthl pierce-point
        theta = 90-baz(istn);                       % baz in degrees from east
        x_ppt = r_ppt*cosd(theta);                  % depthl pierce-point coordinates
        y_ppt = r_ppt*sind(theta);
        
        
%         
        ray_lo = deptho/cosd(inc);                    % straight line stn to depthl pierce-point
        r_ppto = ray_lo*sind(inc);                    % lateral distance between stn and depthl pierce-point
        %theta = 90-baz(istn);                       % baz in degrees from east
        x_ppto = r_ppto*cosd(theta);                  % depthl pierce-point coordinates
        y_ppto = r_ppto*sind(theta);
        
%         
        
        c_v   = [ stn.z(snum) stn.n(snum) stn.ne(snum) stn.e(snum) stn.se(snum) ...
                stn.s(snum) stn.sw(snum) stn.w(snum) stn.nw(snum) ]';
       
            
        m_v   = griddata(c_x, c_y, c_v, x_ppt, y_ppt);          % interp vel. for pierce point
         
        if m_v == 0                                             %
            m_v = (sum(stn_z(stn_z>0))/n_z);                    % for stns w/o good crust time
        end                                                     % use mean vel and stn elv.
         m_vo   = griddata(c_xo, c_yo, c_v, x_ppto, y_ppto);
         if m_vo == 0                                             %
             m_vo = (sum(stn_z(stn_z>0))/n_z);                    % for stns w/o good crust time
         end  
        
        c_t   = ray_l/m_v;                             % crust time
        c_to   = ray_l/m_vo; 
        %M%elv_t = stn.elv(snum)/6.5;                    % elevation time (very conservative)
        elv_t = stn.elv(snum)/4.05;                        % only if Moho depthl
                                                       % not crust
                                                       % thickness
         elv_to = stn.elv(snum)/4.0;                                     
       %M% ray.corr1(indx(istn)) = c_t; % + elv_t;                 % raw time z=35->stn 
       if stn.elv <0
       ray.corr1(indx(istn)) = (c_to + elv_to);  
       else
        ray.corr1(indx(istn)) = (c_t + elv_t);
       end
    end
    
    ray.corr1(indx) = ray.corr1(indx) - mean(ray.corr1(indx));  % demean event corrections
%%%%i added this
%this bit here is for plotting the crustal corrections
kount=kount+1;
lat_ppt(kount) = stn.lat(snum)+(y_ppt/110);                    %latitude of the piercing point
lon_ppt(kount) = stn.lon(snum)+(x_ppt/110);                    %longitude of the piercing point
corr_4plot(kount) = ray.corr1(indx(istn));                     % for plotting, I want it without the demeaning
corr_sta(kount,:) = stn.sta(snum,:);
%

end
close( h );

%i added this
%this bit here creates the file for plotting
corr_4plot = (corr_4plot - mean(corr_4plot));
myfid=fopen('corr_4plot.txt','wt');
for k=1:kount
fprintf(myfid,'%2.2f %2.2f %1.4f %s\n', lat_ppt(k), lon_ppt(k), corr_4plot(k), corr_sta(k,:));
end

fclose(myfid);
%

data.ray = ray;